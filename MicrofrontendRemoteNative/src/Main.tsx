import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const styles = StyleSheet.create({
  pageContainer: {
    backgroundColor: 'grey',
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 30,
    color: 'white',
    textAlign: 'center',
  },
});

const Main = () => {
  return (
    <View style={styles.pageContainer}>
      <Text style={styles.title}>
        This is a remote react native microfrontend!
      </Text>
    </View>
  );
};

export default Main;
