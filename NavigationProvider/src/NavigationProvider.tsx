import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';

const Stack = createNativeStackNavigator();
const NavigationProvider = ({children}: {children: any}) => (
  <NavigationContainer>{children}</NavigationContainer>
);
NavigationProvider.Stack = Stack;

export default NavigationProvider;
