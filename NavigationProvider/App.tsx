import React from 'react';
import {Button, Text, View} from 'react-native';
import NavigationProvider from './src/NavigationProvider';

const Main = ({navigation}) => {
  return (
    <View>
      <Button title="ONE" onPress={() => navigation.navigate('ONE')} />
      <Button title="TWO" onPress={() => navigation.navigate('TWO')} />
    </View>
  );
};

const One = () => {
  return <Text>ONE</Text>;
};

const Two = () => {
  return <Text>ONE</Text>;
};

function App(): JSX.Element {
  return (
    <NavigationProvider>
      <NavigationProvider.Stack.Navigator initialRouteName="Main">
        <NavigationProvider.Stack.Screen name="Main" component={Main} />
        <NavigationProvider.Stack.Screen name="ONE" component={One} />
        <NavigationProvider.Stack.Screen name="TWO" component={Two} />
      </NavigationProvider.Stack.Navigator>
    </NavigationProvider>
  );
}

export default App;
