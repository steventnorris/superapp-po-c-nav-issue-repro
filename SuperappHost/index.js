/**
 * @format
 */

import {AppRegistry, Platform} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import {ScriptManager, Federated} from '@callstack/repack/client';

const resolverRemoteIdentifiers = [
  'MicrofrontendRemoteNative',
  'MicrofrontendRemoteWebview',
  'NavigationProvider',
];

ScriptManager.shared.addResolver(async (scriptId, caller) => {
  const resolveURL = Federated.createURLResolver({
    containers: {
      NavigationProvider: 'http://localhost:8083/[name][ext]',
      MicrofrontendRemoteNative: 'http://localhost:8084/[name][ext]',
      MicrofrontendRemoteWebview: 'http://localhost:8082/[name][ext]',
    },
  });
  console.info(`Starting fetch: ${scriptId} - ${caller}`);
  if (
    resolverRemoteIdentifiers.some(value => {
      return value === scriptId || value === caller;
    })
  ) {
    console.info(`Fetching a package we do expect: ${scriptId} - ${caller}`);
    const url = resolveURL(scriptId, caller);
    if (url) {
      console.info(`Fetching from url ${url}`);
      return {
        url: url,
        query: {
          platform: Platform.OS,
        },
      };
    } else {
      console.error(`Could not find url: ${scriptId} - ${caller}`);
    }
  } else {
    console.warn(
      `Fetching a package we didn't expect: ${scriptId} - ${caller}`,
    );
  }
});

AppRegistry.registerComponent(appName, () => App);
