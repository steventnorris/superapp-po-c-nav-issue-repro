import {Federated} from '@callstack/repack/client';
import React from 'react';
import {Text} from 'react-native';

const MicrofrontendRemoteWebview = React.lazy(() =>
  Federated.importModule('MicrofrontendRemoteWebview', './Main'),
);

const MicrofrontendRemoteWebviewContainer = () => {
  return (
    <React.Suspense
      fallback={<Text>Please hold while we fetch the remote bundle....</Text>}>
      <MicrofrontendRemoteWebview />
    </React.Suspense>
  );
};

export default MicrofrontendRemoteWebviewContainer;
