import {Federated} from '@callstack/repack/client';
import React from 'react';
import {Text} from 'react-native';

const MicrofrontendRemoteNative = React.lazy(() =>
  Federated.importModule('MicrofrontendRemoteNative', './Main'),
);

const MicrofrontendRemoteNativeContainer = () => {
  return (
    <React.Suspense
      fallback={<Text>Please hold while we fetch the remote bundle....</Text>}>
      <MicrofrontendRemoteNative />
    </React.Suspense>
  );
};

export default MicrofrontendRemoteNativeContainer;
