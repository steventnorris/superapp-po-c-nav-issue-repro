import React from 'react';
import {
  Alert,
  Button,
  Linking,
  StyleSheet,
  Text,
  Touchable,
  TouchableOpacity,
  View,
} from 'react-native';

const styles = StyleSheet.create({
  pageContainer: {
    backgroundColor: 'grey',
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 30,
    textAlign: 'center',
    color: 'white',
  },
  buttonContainer: {
    height: 40,
    backgroundColor: 'white',
    borderRadius: 10,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 5,
    paddingRight: 5,
  },
  buttonText: {
    color: 'black',
    fontSize: 20,
  },
});

const Main = ({navigation}) => {
  return (
    <View style={styles.pageContainer}>
      <Text style={styles.title}>This is the superapp landing page.</Text>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('MicrofrontendRemoteNative');
        }}>
        <View style={{...styles.buttonContainer, marginTop: 10}}>
          <Text style={styles.buttonText}>
            Go to Remote Native Microfrontend
          </Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('MicrofrontendRemoteWebview');
        }}>
        <View style={{...styles.buttonContainer, marginTop: 10}}>
          <Text style={styles.buttonText}>
            Go to Remote Webview Microfrontend
          </Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={async () => {
          const canOpen = await Linking.canOpenURL(
            'https://play.google.com/store/apps/developer?id=Tractor+Supply+Company',
          );
          if (canOpen) {
            await Linking.openURL(
              'https://play.google.com/store/apps/developer?id=Tractor+Supply+Company',
            );
          } else {
            Alert.alert('Cannot open link.');
          }
        }}>
        <View style={{...styles.buttonContainer, marginTop: 10}}>
          <Text style={styles.buttonText}>
            Go to Google Play Store Deep Link
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default Main;
