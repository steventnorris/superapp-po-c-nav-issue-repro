import React, {useEffect, useState} from 'react';
import Main from './src/host_core/Main';
import {Federated} from '@callstack/repack/client';
import MicrofrontendRemoteNativeContainer from './src/microfrontend_containers/MicrofrontendRemoteNative';
import MicrofrontendRemoteWebviewContainer from './src/microfrontend_containers/MicrofrontendRemoteWebview';
import {Text} from 'react-native';

function App(): JSX.Element {
  // WORKING CODE COMMENTED OUT
  // const [NavigationProvider, setNavigationProvider] = useState<
  //   {provider: any; stack: any} | undefined
  // >(undefined);

  // useEffect(() => {
  //   if (!NavigationProvider) {
  //     Federated.importModule('NavigationProvider', './NavigationProvider')
  //       .then(providerValue => {
  //         console.log('GOT PROVIDER');
  //         console.log(providerValue);
  //         setNavigationProvider({provider: providerValue.default, stack: {}});
  //       })
  //       .catch(error => {
  //         console.log('FAILED PROVIDER');
  //         console.error(error);
  //       });
  //   }
  // });

  const NavigationProvider = React.lazy(() =>
    Federated.importModule('NavigationProvider', './NavigationProvider'),
  );

  return (
    <React.Suspense
      fallback={
        <Text>Hold tight while we fetch the navigation provider...</Text>
      }>
      <NavigationProvider>
        <NavigationProvider.Stack.Navigator initialRouteName="Main">
          <NavigationProvider.Stack.Screen name="Main" component={Main} />
          <NavigationProvider.Stack.Screen
            name="MicrofrontendRemoteNative"
            component={MicrofrontendRemoteNativeContainer}
          />
          <NavigationProvider.Stack.Screen
            name="MicrofrontendRemoteWebview"
            component={MicrofrontendRemoteWebviewContainer}
          />
        </NavigationProvider.Stack.Navigator>
      </NavigationProvider>
    </React.Suspense>
    // WORKING CODE COMMENTED OUT
    // <>
    //   {!NavigationProvider ? (
    //     <Text>Hold tight while we fetch the navigation provider...</Text>
    //   ) : (
    // <NavigationProvider.provider>
    //   <NavigationProvider.provider.Stack.Navigator initialRouteName="Main">
    //     <NavigationProvider.provider.Stack.Screen
    //       name="Main"
    //       component={Main}
    //     />
    //     <NavigationProvider.provider.Stack.Screen
    //       name="MicrofrontendRemoteNative"
    //       component={MicrofrontendRemoteNativeContainer}
    //     />
    //     <NavigationProvider.provider.Stack.Screen
    //       name="MicrofrontendRemoteWebview"
    //       component={MicrofrontendRemoteWebviewContainer}
    //     />
    //   </NavigationProvider.provider.Stack.Navigator>
    // </NavigationProvider.provider>
    //   )}
    // </>
  );
}

export default App;
