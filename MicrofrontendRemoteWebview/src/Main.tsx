import React from 'react';
import WebView from 'react-native-webview';

const Main = () => {
  return <WebView source={{uri: 'https://www.tractorsupply.com/'}} />;
};

export default Main;
